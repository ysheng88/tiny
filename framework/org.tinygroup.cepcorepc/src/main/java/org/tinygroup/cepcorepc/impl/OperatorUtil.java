/**
 *  Copyright (c) 1997-2013, www.tinygroup.org (luo_guo@icloud.com).
 *
 *  Licensed under the GPL, Version 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.gnu.org/licenses/gpl.html
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.tinygroup.cepcorepc.impl;


public class OperatorUtil {
	public static String AR_TO_SC_SERVICE_KEY = "ar_regto_sc_services";
	public static String SC_TO_AR_SERVICE_KEY = "sc_regto_ar_services";
	public static String SC_TO_AR_PATH_KEY = "sc_regto_ar_path";
	public static String NODE_KEY = "node_key";
	public static String NODE_PATH = "node_path";
	public static String TYPE_KEY = "type_key";
	public static String REG_KEY = "type_reg";
	public static String UNREG_KEY = "type_unreg";
	
}
